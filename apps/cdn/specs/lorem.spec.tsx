import { render } from '@testing-library/react';
import React from 'react';
import Lorem from '../pages/lorem';

describe('Lorem', () => {
  it('should have a button', () => {
    const { getByRole } = render(<Lorem />);

    expect(getByRole('button')).toBeTruthy();
  });
});
