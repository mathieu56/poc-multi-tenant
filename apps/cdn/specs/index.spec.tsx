import { render } from '@testing-library/react';
import React from 'react';
import Index from '../pages/index';

describe('Index', () => {
  it('should have a button', () => {
    const { getByRole } = render(<Index />);

    expect(getByRole('button')).toBeTruthy();
  });
});
