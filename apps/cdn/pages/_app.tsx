import { AppProps } from 'next/app';
import Head from 'next/head';
import React from 'react';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Simple Page</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />

        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Lato&family=Viaoda+Libre&display=swap"
          rel="stylesheet"
        />
        <link href="styles.css" rel="stylesheet" />
        <link href="http://localhost:3333/css-cdn" rel="stylesheet" />
      </Head>
      <div className="app">
        <main>
          <Component {...pageProps} />
        </main>
      </div>
    </>
  );
}

export default CustomApp;
