import Link from 'next/link';
import React from 'react';

export function Index() {
  return (
    <div className="background">
      <div>
        <span>Simple page</span>
        <Link href="/lorem">
          <button>Next page</button>
        </Link>
      </div>
    </div>
  );
}

export default Index;
