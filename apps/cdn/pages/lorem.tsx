import Link from 'next/link';
import React from 'react';

export function Lorem() {
  return (
    <div className="background">
      <div>
        <span className="long-text">Lorem ipsum</span>
        <Link href="/">
          <button>Previous page</button>
        </Link>
      </div>
    </div>
  );
}

export default Lorem;
