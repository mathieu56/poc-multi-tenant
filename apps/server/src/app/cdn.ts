import * as express from 'express';
import * as path from 'path';

const router = express.Router();

router.get('/', (req, res) => {
  const cssFile = 'assets/styles.css';
  // use resolve path to avoid __dirname that not working in tests
  const filePath = path.resolve('apps/server/src/' + cssFile);

  res.sendFile(filePath);
});

export default router;
