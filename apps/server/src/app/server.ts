import * as express from 'express';
import cdnRouter from './cdn';

const app = express();

app.get('/api', (req, res) => {
  res.send({ message: 'Welcome to server!' });
});

app.use('/css-cdn', cdnRouter);

export default app;
