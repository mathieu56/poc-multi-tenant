import * as request from 'supertest';
import app from '../app/server';

describe('css cdn', () => {
  it('should get a css file', async () => {
    const res = await request(app).get('/css-cdn/');

    expect(res.type).toMatch(/css/);
    expect(res.status).toBe(200);
  });
});
